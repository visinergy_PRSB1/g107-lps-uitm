-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2020 at 05:22 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `machine_a`
--

-- --------------------------------------------------------

--
-- Table structure for table `actual_sequence`
--

CREATE TABLE `actual_sequence` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `part_no_id` int(11) NOT NULL,
  `qty_plan` varchar(20) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `actual_time` varchar(20) NOT NULL,
  `qty_total` varchar(20) NOT NULL,
  `gsph` varchar(20) NOT NULL,
  `total_dt` varchar(20) NOT NULL,
  `dies_change` varchar(20) NOT NULL,
  `dies_change_dt` varchar(20) NOT NULL,
  `no_production_dt` varchar(20) NOT NULL,
  `production_dt` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `actual_sequence`
--

INSERT INTO `actual_sequence` (`id`, `date`, `time`, `part_no_id`, `qty_plan`, `start_time`, `end_time`, `actual_time`, `qty_total`, `gsph`, `total_dt`, `dies_change`, `dies_change_dt`, `no_production_dt`, `production_dt`) VALUES
(1, '2020-03-25', '15:06:50', 1, '35', '2020-03-25 15:06:50', '2020-03-25 15:08:00', '70.0', '3', '0.1', '', '0.000000', '0.000000', '16.000000', '0.000000'),
(2, '2020-03-25', '15:08:00', 2, '35', '2020-03-25 15:08:00', '2020-03-26 16:24:03', '90963.0', '3', '0.1', '', '46.000000', '0.000000', '0.000000', '0.000000'),
(3, '2020-03-26', '16:24:03', 2, '100', '2020-03-26 16:24:03', '2020-05-22 18:23:23', '4931960.0', '13', '0.1', '', '', '', '', ''),
(4, '2020-05-22', '18:23:23', 5, '25', '2020-05-22 18:23:23', '2020-05-22 18:40:03', '1000.0', '13', '0.1', '', '', '', '', ''),
(5, '2020-05-22', '18:40:03', 3, '50', '2020-05-22 18:40:03', '2020-05-27 11:37:47', '406664.0', '13', '0.1', '', '', '', '', ''),
(6, '2020-05-27', '11:37:47', 2, '15', '2020-05-27 11:37:47', '2020-05-27 12:55:01', '4634.0', '13', '0.1', '', '', '', '', ''),
(7, '2020-05-27', '12:55:01', 7, '40', '2020-05-27 12:55:01', '2020-05-27 12:56:20', '79.0', '13', '0.1', '', '', '', '', ''),
(8, '2020-05-27', '12:56:20', 10, '35', '2020-05-27 12:56:20', '2020-05-27 12:59:20', '180.0', '13', '0.1', '', '', '', '', ''),
(9, '2020-05-27', '12:59:20', 31, '70', '2020-05-27 12:59:20', '2020-05-27 13:04:24', '304.0', '13', '0.1', '', '', '', '', ''),
(10, '2020-05-27', '13:04:24', 30, '20', '2020-05-27 13:04:24', '2020-05-27 13:14:42', '618.0', '13', '0.1', '', '0.000000', '0.000000', '4060.000000', '0.000000'),
(11, '2020-05-27', '13:14:42', 28, '20', '2020-05-27 13:14:42', '2020-05-28 21:39:50', '116708.0', '10', '0.0', '', '0.000000', '0.000000', '108.000000', '3819.000000'),
(12, '2020-05-28', '21:39:50', 2, '20', '2020-05-28 21:39:50', '2020-05-30 12:49:14', '140964.0', '20', '1.4', '3447.0', '', '', '', ''),
(13, '2020-05-30', '12:49:14', 2, '4', '2020-05-30 12:49:14', '0000-00-00 00:00:00', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `daily_gsph`
--

CREATE TABLE `daily_gsph` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `shift_id` int(20) NOT NULL,
  `part_no_id` int(10) NOT NULL,
  `current_gsph` varchar(10) NOT NULL,
  `ave_current_gsph` varchar(10) NOT NULL,
  `ave_daily_gsph` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `derive`
--

CREATE TABLE `derive` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `shift_id` int(20) NOT NULL,
  `part_no_id` int(10) NOT NULL,
  `qty_plan` varchar(20) NOT NULL,
  `qty_actual` varchar(20) DEFAULT '0',
  `qty_ok` varchar(20) DEFAULT '0',
  `qty_reject` varchar(20) DEFAULT '0',
  `qty_achievement` varchar(20) DEFAULT '0',
  `utilization` varchar(20) DEFAULT '0',
  `gsph` varchar(20) DEFAULT '0',
  `oee` varchar(20) DEFAULT '0',
  `availability` varchar(20) DEFAULT '0',
  `max_speed` varchar(20) DEFAULT '0',
  `actual_speed` varchar(15) DEFAULT '0',
  `speed_loss` varchar(15) DEFAULT '0',
  `yield` varchar(20) DEFAULT '0',
  `good_footage` varchar(15) DEFAULT '0',
  `rejection_footage` varchar(15) DEFAULT '0',
  `total_dt` varchar(10) DEFAULT '0',
  `speed` varchar(5) DEFAULT '0',
  `start_time` time NOT NULL,
  `qty_rework` varchar(25) NOT NULL,
  `qty_scrap` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `derive`
--

INSERT INTO `derive` (`id`, `date`, `time`, `shift_id`, `part_no_id`, `qty_plan`, `qty_actual`, `qty_ok`, `qty_reject`, `qty_achievement`, `utilization`, `gsph`, `oee`, `availability`, `max_speed`, `actual_speed`, `speed_loss`, `yield`, `good_footage`, `rejection_footage`, `total_dt`, `speed`, `start_time`, `qty_rework`, `qty_scrap`) VALUES
(1, '2020-03-25', '15:07:28', 1, 1, '35', '3', '2.0', '1.0', '8.6', '1.0', '0.1', '0.0', '100.0', '24.3', '30.0', '-5.7', '0.0', '30.0', '0.0', '0.0', '1.2', '15:06:50', '', ''),
(2, '2020-03-26', '16:27:36', 2, 1, '100', '13', '10.0', '3.0', '13.0', '8.6', '0.1', '0.0', '100.0', '207.3', '0.0', '207.3', '0.0', '0.0', '0.0', '0.0', '0.0', '16:24:03', '', ''),
(3, '2020-05-27', '14:32:12', 2, 1, '20', '10', '6.0', '4.0', '50.0', '190.5', '0.0', '0.0', '16.2', '741.0', '100.0', '641.0', '0.0', '60.0', '40.0', '3832.0', '0.1', '13:14:42', '', ''),
(4, '2020-05-28', '00:00:00', 3, 1, '20', '20', '5.0', '15.0', '100.0', '56.7', '1.4', '0.0', '45.0', '35.3', '56.8', '24.7', '1.0', '75.0', '25.0', '3447.0', '3.2', '08:20:13', '', ''),
(5, '0000-00-00', '00:00:00', 3, 2, '4', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '00:00:00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `downtime_report`
--

CREATE TABLE `downtime_report` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `part_no_id` int(7) NOT NULL,
  `shift_id` int(7) NOT NULL,
  `time` time NOT NULL,
  `downtime_period` varchar(20) NOT NULL,
  `problem_dies_change` int(11) NOT NULL,
  `problem_die` int(11) NOT NULL,
  `problem_machine` int(11) NOT NULL,
  `problem_material` int(11) NOT NULL,
  `problem_production` int(11) NOT NULL,
  `problem_qc` int(11) NOT NULL,
  `pic_id` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `downtime_report`
--

INSERT INTO `downtime_report` (`id`, `date`, `part_no_id`, `shift_id`, `time`, `downtime_period`, `problem_dies_change`, `problem_die`, `problem_machine`, `problem_material`, `problem_production`, `problem_qc`, `pic_id`) VALUES
(1, '2020-05-22', 5, 1, '18:27:34', '00:01:18', 0, 0, 0, 0, 4, 0, 3),
(2, '2020-05-22', 5, 1, '18:28:20', '00:00:46', 0, 0, 5, 0, 0, 0, 1),
(3, '2020-05-22', 5, 1, '18:31:40', '00:03:19', 0, 0, 0, 0, 1, 0, 3),
(4, '2020-05-22', 5, 1, '18:32:51', '00:01:09', 0, 3, 0, 0, 0, 0, 4),
(5, '2020-05-22', 5, 1, '18:33:38', '00:00:46', 0, 0, 0, 0, 6, 0, 3),
(6, '2020-05-22', 5, 1, '18:33:58', '00:00:21', 0, 1, 0, 0, 0, 0, 4),
(7, '2020-05-22', 5, 1, '18:34:24', '00:00:26', 0, 0, 0, 0, 7, 0, 3),
(8, '2020-05-22', 5, 1, '18:35:00', '00:00:35', 0, 0, 0, 0, 0, 1, 6),
(9, '2020-05-22', 5, 1, '18:35:33', '00:00:32', 0, 0, 0, 0, 3, 0, 3),
(10, '2020-05-22', 5, 1, '18:35:58', '00:00:25', 0, 0, 0, 5, 0, 0, 5),
(11, '2020-05-22', 5, 1, '18:39:36', '00:03:37', 0, 0, 0, 0, 2, 0, 3),
(12, '2020-05-27', 2, 1, '11:38:55', '00:00:07', 0, 0, 0, 0, 26, 0, 3),
(13, '2020-05-27', 2, 1, '11:39:23', '00:00:27', 0, 6, 0, 0, 0, 0, 2),
(14, '2020-05-27', 2, 1, '11:44:06', '00:04:33', 0, 0, 0, 0, 1, 0, 3),
(15, '2020-05-27', 2, 1, '11:58:58', '00:05:02', 4, 0, 0, 0, 0, 0, 4),
(16, '2020-05-27', 2, 1, '12:04:03', '00:04:13', 0, 0, 0, 0, 9, 0, 3),
(17, '2020-05-27', 28, 2, '13:17:18', '00:00:27', 0, 0, 0, 0, 3, 0, 3),
(18, '2020-05-27', 28, 2, '13:20:05', '00:00:04', 0, 0, 0, 0, 20, 0, 3),
(19, '2020-05-27', 28, 2, '13:20:17', '00:00:13', 0, 16, 0, 0, 0, 0, 2),
(20, '2020-05-27', 28, 2, '14:23:26', '01:03:08', 0, 0, 0, 0, 17, 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fp_setting`
--

CREATE TABLE `fp_setting` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `part_no_id` int(25) NOT NULL,
  `shift_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fp_setting`
--

INSERT INTO `fp_setting` (`id`, `date`, `part_no_id`, `shift_id`) VALUES
(1, '2020-05-30', 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `machine_info`
--

CREATE TABLE `machine_info` (
  `id` int(11) NOT NULL,
  `organization` varchar(255) NOT NULL,
  `factory` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `machine_name` varchar(255) NOT NULL,
  `machine_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `real_time`
--

CREATE TABLE `real_time` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `plan_production` varchar(20) DEFAULT '00:00:00',
  `no_production` varchar(20) DEFAULT '00:00:00',
  `dies_change` varchar(20) DEFAULT '00:00:00',
  `production_dt` varchar(20) DEFAULT '00:00:00',
  `dies_change_dt` varchar(20) DEFAULT '00:00:00',
  `machine_dt` varchar(20) DEFAULT '00:00:00',
  `dies_dt` varchar(20) DEFAULT '00:00:00',
  `quality_dt` varchar(20) DEFAULT '00:00:00',
  `material_dt` varchar(20) DEFAULT '00:00:00',
  `part_no_id` int(11) NOT NULL,
  `qty_plan` varchar(20) NOT NULL,
  `shift_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `real_time`
--

INSERT INTO `real_time` (`id`, `date`, `time`, `plan_production`, `no_production`, `dies_change`, `production_dt`, `dies_change_dt`, `machine_dt`, `dies_dt`, `quality_dt`, `material_dt`, `part_no_id`, `qty_plan`, `shift_id`) VALUES
(1, '2020-03-25', '15:06:56', '00:00:00', '00:00:16', '00:00:00', '00:00:40', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 1, '35', 1),
(2, '2020-03-25', '15:07:20', '00:00:24', '00:00:00', '00:00:00', '00:00:45', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 1, '35', 1),
(3, '2020-03-25', '15:08:07', '00:00:00', '00:00:00', '00:00:46', '00:00:45', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 2, '35', 1),
(4, '2020-03-25', '15:08:48', '00:00:40', '00:00:00', '00:00:00', '00:00:30', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 2, '35', 1),
(5, '2020-03-26', '16:24:08', '00:00:00', '01:07:40', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 2, '100', 2),
(6, '2020-03-26', '16:27:30', '00:03:20', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 2, '100', 2),
(7, '2020-05-22', '18:25:21', '00:00:00', '00:02:38', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 5, '25', 1),
(8, '2020-05-22', '18:27:34', '00:00:00', '00:00:00', '00:00:00', '00:01:18', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 5, '25', 1),
(9, '2020-05-22', '18:28:20', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:46', '00:00:00', '00:00:00', '00:00:00', 5, '25', 1),
(10, '2020-05-22', '18:31:40', '00:00:00', '00:00:00', '00:00:00', '00:03:19', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 5, '25', 1),
(11, '2020-05-22', '18:32:51', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:01:09', '00:00:00', '00:00:00', 5, '25', 1),
(12, '2020-05-22', '18:33:38', '00:00:00', '00:00:00', '00:00:00', '00:00:46', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 5, '25', 1),
(13, '2020-05-22', '18:33:58', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:21', '00:00:00', '00:00:00', 5, '25', 1),
(14, '2020-05-22', '18:34:24', '00:00:00', '00:00:00', '00:00:00', '00:00:26', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 5, '25', 1),
(15, '2020-05-22', '18:35:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:35', '00:00:00', 5, '25', 1),
(16, '2020-05-22', '18:35:33', '00:00:00', '00:00:00', '00:00:00', '00:00:32', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 5, '25', 1),
(17, '2020-05-22', '18:35:58', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:25', 5, '25', 1),
(18, '2020-05-22', '18:39:36', '00:14:14', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 5, '25', 1),
(19, '2020-05-22', '18:39:36', '00:00:00', '00:00:00', '00:00:00', '00:03:37', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 5, '25', 1),
(20, '2020-05-27', '11:37:57', '00:00:00', '00:00:48', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 2, '15', 1),
(21, '2020-05-27', '11:38:55', '00:00:00', '00:00:00', '00:00:00', '00:00:07', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 2, '15', 1),
(22, '2020-05-27', '11:39:23', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:27', '00:00:00', '00:00:00', 2, '15', 1),
(23, '2020-05-27', '11:44:06', '00:05:37', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 2, '15', 1),
(24, '2020-05-27', '11:44:06', '00:00:00', '00:00:00', '00:00:00', '00:04:33', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 2, '15', 1),
(25, '2020-05-27', '11:58:58', '00:00:00', '00:00:00', '00:15:02', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 2, '15', 1),
(26, '2020-05-27', '11:58:58', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:05:02', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 2, '15', 1),
(27, '2020-05-27', '12:04:03', '00:05:04', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 2, '15', 1),
(28, '2020-05-27', '12:04:03', '00:00:00', '00:00:00', '00:00:00', '00:04:13', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 2, '15', 1),
(29, '2020-05-27', '13:15:59', '00:00:00', '00:01:48', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 28, '20', 2),
(30, '2020-05-27', '13:17:18', '00:00:00', '00:00:00', '00:00:00', '00:00:27', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 28, '20', 2),
(31, '2020-05-27', '13:20:05', '00:00:00', '00:00:00', '00:00:00', '00:00:04', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 28, '20', 2),
(32, '2020-05-27', '13:20:17', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:13', '00:00:00', '00:00:00', 28, '20', 2),
(33, '2020-05-27', '14:23:26', '01:07:26', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 28, '20', 2),
(34, '2020-05-27', '14:23:26', '00:00:00', '00:00:00', '00:00:00', '01:03:08', '00:00:00', '00:00:00', '00:00:00', '00:00:00', '00:00:00', 28, '20', 2);

-- --------------------------------------------------------

--
-- Table structure for table `rework_report`
--

CREATE TABLE `rework_report` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `part_no_id` int(7) NOT NULL,
  `shift_id` int(7) NOT NULL,
  `time` time NOT NULL,
  `crack` varchar(255) NOT NULL,
  `necking` varchar(255) NOT NULL,
  `dented` varchar(255) NOT NULL,
  `wrinkle` varchar(255) NOT NULL,
  `seizure` varchar(255) NOT NULL,
  `mekure` varchar(255) NOT NULL,
  `burr` varchar(255) NOT NULL,
  `dimension_ng` varchar(255) NOT NULL,
  `other` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rework_report`
--

INSERT INTO `rework_report` (`id`, `date`, `part_no_id`, `shift_id`, `time`, `crack`, `necking`, `dented`, `wrinkle`, `seizure`, `mekure`, `burr`, `dimension_ng`, `other`) VALUES
(1, '2020-03-24', 5, 1, '16:51:14', '1', '2', '0', '0', '0', '0', '0', '1', '1'),
(2, '2020-03-24', 5, 1, '16:51:51', '1', '0', '0', '0', '0', '0', '0', '1', '1'),
(3, '2020-03-24', 5, 1, '16:52:35', '1', '1', '1', '1', '1', '1', '1', '1', '0'),
(4, '2020-03-24', 40, 1, '16:57:41', '1', '0', '0', '0', '0', '0', '0', '0', '1'),
(5, '2020-03-24', 40, 1, '16:58:57', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(6, '2020-03-24', 40, 1, '16:59:42', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
(7, '2020-03-24', 41, 1, '07:27:21', '0', '1', '1', '1', '1', '0', '0', '0', '0'),
(8, '2020-03-25', 1, 1, '08:10:32', '5', '5', '5', '5', '5', '5', '5', '5', '5'),
(9, '2020-03-25', 1, 2, '08:13:58', '3', '3', '3', '3', '3', '3', '3', '3', '3'),
(10, '2020-03-25', 1, 2, '09:20:31', '0', '0', '1', '0', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `scrap_report`
--

CREATE TABLE `scrap_report` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `part_no_id` int(7) NOT NULL,
  `shift_id` int(7) NOT NULL,
  `time` time NOT NULL,
  `crack` varchar(255) NOT NULL,
  `necking` varchar(255) NOT NULL,
  `dented` varchar(255) NOT NULL,
  `wrinkle` varchar(255) NOT NULL,
  `seizure` varchar(255) NOT NULL,
  `mekure` varchar(255) NOT NULL,
  `burr` varchar(255) NOT NULL,
  `dimension_ng` varchar(255) NOT NULL,
  `other` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `scrap_report`
--

INSERT INTO `scrap_report` (`id`, `date`, `part_no_id`, `shift_id`, `time`, `crack`, `necking`, `dented`, `wrinkle`, `seizure`, `mekure`, `burr`, `dimension_ng`, `other`) VALUES
(1, '2020-03-24', 5, 1, '16:51:05', '0', '1', '1', '1', '0', '0', '0', '0', '0'),
(2, '2020-03-24', 41, 1, '07:27:37', '1', '1', '1', '2', '3', '5', '1', '1', '1'),
(3, '2020-03-24', 5, 1, '07:36:59', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(4, '2020-03-25', 1, 1, '08:08:28', '1', '1', '1', '1', '1', '1', '1', '1', '1'),
(5, '2020-03-25', 1, 2, '08:12:12', '2', '2', '2', '2', '2', '2', '2', '2', '2'),
(6, '2020-03-25', 1, 2, '08:14:33', '3', '3', '3', '3', '3', '3', '3', '3', '3'),
(7, '2020-03-25', 1, 2, '09:20:22', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
(8, '2020-03-25', 1, 1, '09:38:04', '0', '5', '5', '0', '0', '0', '0', '0', '0'),
(9, '2020-03-25', 1, 1, '15:07:28', '0', '1', '1', '1', '0', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `type_defect`
--

CREATE TABLE `type_defect` (
  `id` int(11) NOT NULL,
  `defect_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_defect`
--

INSERT INTO `type_defect` (`id`, `defect_type`) VALUES
(1, 'Necking'),
(2, 'Dented'),
(3, 'Wrinkle'),
(4, 'Seizure'),
(5, 'Mekure'),
(6, 'Burr'),
(7, 'Dimension_NG'),
(8, 'Other'),
(9, 'Crack');

-- --------------------------------------------------------

--
-- Table structure for table `type_department`
--

CREATE TABLE `type_department` (
  `id` int(7) NOT NULL,
  `department_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_department`
--

INSERT INTO `type_department` (`id`, `department_name`) VALUES
(1, 'machine'),
(2, 'dies'),
(3, 'production'),
(4, 'dies change'),
(5, 'material'),
(6, 'qc');

-- --------------------------------------------------------

--
-- Table structure for table `type_dies_change_downtime`
--

CREATE TABLE `type_dies_change_downtime` (
  `id` int(11) NOT NULL,
  `dies_change_downtime` varchar(20) DEFAULT NULL,
  `department_id` int(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_dies_change_downtime`
--

INSERT INTO `type_dies_change_downtime` (`id`, `dies_change_downtime`, `department_id`) VALUES
(1, 'Bolt and nut not eno', 4),
(2, 'Spanner not enough', 4),
(3, 'Moving bolster probl', 4),
(4, 'Unskill manpower', 4);

-- --------------------------------------------------------

--
-- Table structure for table `type_die_downtime`
--

CREATE TABLE `type_die_downtime` (
  `id` int(11) NOT NULL,
  `die_downtime` varchar(20) DEFAULT NULL,
  `department_id` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_die_downtime`
--

INSERT INTO `type_die_downtime` (`id`, `die_downtime`, `department_id`) VALUES
(1, 'Part Mekure', 2),
(2, 'Part Dented', 2),
(3, 'Scrap Stuck', 2),
(4, 'Wrinkle & Overlap', 2),
(5, 'Part Burr', 2),
(6, 'Part Seizure', 2),
(7, 'Hole ng', 2),
(8, 'Part Stuck', 2),
(9, 'Part Crack & Necking', 2),
(10, 'Die Mark', 2),
(11, 'Jig Out Stg', 2),
(12, 'Cutter Ng', 2),
(13, 'Adjust Guide', 2),
(14, 'Guide Broken', 2),
(15, 'Put oil', 2),
(16, 'Part unbalance', 2),
(17, 'Clean die', 2),
(18, 'Marking problem', 2),
(19, 'Flange NG', 2),
(20, 'Pilot pin problem', 2),
(21, 'Overlap', 2),
(22, 'Punch broken', 2),
(23, 'Part checking', 2),
(24, 'Block problem', 2),
(25, 'Stopper problem', 2),
(26, 'Part Scratch', NULL),
(27, 'Finger problem', 2);

-- --------------------------------------------------------

--
-- Table structure for table `type_machine_downtime`
--

CREATE TABLE `type_machine_downtime` (
  `id` int(11) NOT NULL,
  `machine_downtime_cause` varchar(255) DEFAULT NULL,
  `department_id` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_machine_downtime`
--

INSERT INTO `type_machine_downtime` (`id`, `machine_downtime_cause`, `department_id`) VALUES
(1, 'Machine over run', 1),
(2, 'Machine control fault', 1),
(3, 'Cant reset fault', 1),
(4, 'Moving bolster prob', 1),
(5, 'Bolster up down', 1),
(6, 'Air pressure inconsistent', 1),
(7, 'Hydraulic oil leak', 1),
(8, 'Main motor problem', 1),
(9, 'Clamper NG', 1),
(10, 'Clutch brake', 1),
(11, 'Overload', 1),
(12, 'Push button emergency', 1),
(13, 'Push button stamp', 1),
(14, 'Slide adjustment problem', 1),
(15, 'Lube Fault', 1),
(16, 'Gate problem', 1),
(17, 'Coil car problem', 1),
(18, 'Machine slow', 1),
(19, 'Pillar problem', 1),
(20, 'Autorun problem', 1),
(21, 'Telescopic conveyor', 1),
(22, 'Top up oil', 1),
(23, 'Feeder problem', 1),
(24, 'Scrap hooper', 1),
(25, 'Brake fault', 1),
(26, 'Board problem', NULL),
(27, 'Control panel trip', NULL),
(28, 'Machine trip', NULL),
(29, 'Safety fence', NULL),
(30, 'Belting problem', NULL),
(31, 'Lamp problem', NULL),
(32, 'Mc slide adjustment prob (meter)', NULL),
(33, 'Mc slide adjustment prob (button)', NULL),
(34, 'S/Height problem', NULL),
(35, 'Robot problem', NULL),
(36, 'Destacker problem', NULL),
(37, 'Emergency stop problem', NULL),
(38, 'Interpress conveyor', NULL),
(39, 'Machine trial', NULL),
(40, 'Sensor problem', NULL),
(41, 'Cushion pressure', NULL),
(42, 'Machine repair online', NULL),
(43, 'Overhead crane (OHC)', NULL),
(44, 'Straightener Prob [Blkg]', NULL),
(45, 'Machine roller prob [Blkg]', NULL),
(46, 'Machine cannot stamp', NULL),
(47, 'Machine stop on off', NULL),
(48, 'Safety plug prob', NULL),
(49, 'Scrap conveyor NG', NULL),
(50, 'Manual remove scrap', NULL),
(51, 'Scrap stuck', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `type_material_downtime`
--

CREATE TABLE `type_material_downtime` (
  `id` int(11) NOT NULL,
  `material_downtime` varchar(20) DEFAULT NULL,
  `department_id` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_material_downtime`
--

INSERT INTO `type_material_downtime` (`id`, `material_downtime`, `department_id`) VALUES
(1, 'Material problem (su', 5),
(2, 'Waiting material (sh', 5),
(3, 'Material scratch', 5),
(4, 'Material cutting NG', 5),
(5, 'Material bending', 5),
(6, 'Material dented', 5);

-- --------------------------------------------------------

--
-- Table structure for table `type_part_list`
--

CREATE TABLE `type_part_list` (
  `id` int(11) NOT NULL,
  `part_no` varchar(255) DEFAULT NULL,
  `part_name` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `process` varchar(255) DEFAULT NULL,
  `line` varchar(255) DEFAULT NULL,
  `cycle_time_sec` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_part_list`
--

INSERT INTO `type_part_list` (`id`, `part_no`, `part_name`, `model`, `process`, `line`, `cycle_time_sec`) VALUES
(1, 'PC811000', 'Sup Rad Upr', 'D46T', '1~4\n', 'TB', 10),
(2, 'PC386001', 'Brkt LH (NCAP)', 'D46D/D01D', '1~5\n', 'TB', 10),
(3, 'PC390000', 'Mbr LH', 'D46D/D01D', '1~5\n', 'TB', 10),
(4, 'PC389000', 'Mbr RH', 'D46D/D01D', '1~5\n', 'TB', 10),
(5, 'PB387/8000', 'Pnl Roof Side Inr Rear L/R', 'D46T', '1~4\n', 'TB', 10),
(6, 'PF367/8000', 'Apron Frt Fender L/R', 'D63D/D88N', '1~4\n', 'TB', 10),
(7, 'PF302000', 'X-Mbr Lwr', 'D63D/D88N', '1~4\n', 'TB', 10),
(8, 'PF301000', 'X-Mbr Upr', 'D63D/D88N', '1~5\n', 'TB', 10),
(9, 'AC323002', 'Side Mbr Lwr LH', 'SAGA R', '1~4\n', 'TB', 10),
(10, 'AC322000', 'Side Mbr Lwr RH', 'SAGA R', '1~5\n', 'TB', 10),
(11, 'AC320000', 'Side Mbr Upr RH', 'SAGA R', '1~5\n', 'TB', 10),
(12, 'PC885001', 'Brkt RH (NCAP)', 'D46D/D01D', '1~5\n', 'TB', 10),
(13, 'PF853/4000', 'Brace Qtr Panel L/R', 'D63D', '1~4\n', 'TB', 10),
(14, 'PG301000', 'Cross member upper', 'D20 N', '1~5\n', 'TB', 10),
(15, 'PG302000', 'Cross member lower', 'D20 N', '1~4\n', 'TB', 10),
(16, 'PG342000', 'Member Fr Panel', 'D20 N', '1~4\n', 'TB', 10),
(17, 'PG322000', 'PANEL COWL TOP INNER', 'D20 N', '1~4\n', 'TB', 10),
(18, 'PG823/824000', 'EXTENTION QUARTER PANEL OUTER RH/LH', 'D20 N', '1~4\n', 'TB', 10),
(19, 'PG325/326000', 'EXTENTION QUARTER PANEL RR RH/LH', 'D20 N', '1~4\n', 'TB', 10),
(20, 'PG829/830000', 'REINFORCEMENT FR DOOR LOCK RH/LH', 'D20 N', '1~4\n', 'TB', 10),
(21, 'PG355000', 'BRKT FRT SIDE MMBR LH', 'D20 N', '1~4\n', 'TB', 10),
(22, 'PG318/819000', 'PANEL COWL TOP SIDE RH/LH', 'D20 N', '1~4\n', 'TB', 10),
(23, 'PG354000', 'BRKT FRT SIDE MMBR RH', 'D20 N', '1~4\n', 'TB', 10),
(24, 'PG345000', 'Mmbr Frt Side Frt LH', 'D20N', '1~4\n', 'TB', 10),
(25, 'PG344000', 'Mmbr Frt Side Frt RH', 'D20N', '1~4\n', 'TB', 10),
(26, 'PG352000', 'Plate Fr Side Mmbr Otr RH', 'D20N', '1~3\n', 'TB', 10),
(27, 'PG353000', 'Plate Fr Side Mmbr Otr LH', 'D20N', '1~3\n', 'TB', 10),
(28, 'PG331000', 'Plate Fr Spring Support RH', 'D20N', '1~4\n', 'TB', 10),
(29, 'PG332000', 'Plate Fr Spring Support LH', 'D20N', '1~4\n', 'TB', 10),
(30, 'PB379/80001', 'Rail Roof Side Inr L/R', 'D01D', '1~4\n', 'TB', 10),
(31, 'PC313/4000', 'Apron Front Fender L/R', 'D46T/D20N', '1~4\n', 'TB', 10),
(32, 'PH327000', 'PANEL COWL TOP INNER', 'D38L', '1~4\n', 'TB', 10),
(33, 'PH329000/330000', 'EXT QTR PNL OUTER RH/LH', 'D38L', '1~5\n', 'TB', 10),
(34, 'PH849000/850000', 'PANEL QUARTER WHEEL HOUSE INNER RH/LH', 'D38L', '1~4\n', 'TB', 10),
(35, 'PH338000/339000', 'REINF SEAT BELT ANCHOR NO 2 RH/LH', 'D38L', '1~4\n', 'TB', 10),
(36, 'PH847000/848000', 'REINF ROOF SIDE INNER RH/LH', 'D38L', '1~5\n', 'TB', 10),
(37, 'PH301000', 'CROSSMEMBER FRONT SUSPENSION UPPER', 'D38L', '1~5\n', 'TB', 10),
(38, 'PH303000', 'BRACKET ENG FRONT MOUNTING RH', 'D38L', '1~5\n', 'TB', 10),
(39, 'PH304000', 'BRACKET ENG FRONT MOUNTING LH', 'D38L', '1~5\n', 'TB', 10),
(40, 'TB0001', 'Ext Qtr Pnl Otr L/R', 'D54T', '1200\n', 'TB', 10),
(41, 'TB0002', 'FUEL TANK UPPER', 'SAGA R', '1~4\n', 'TB', 10),
(42, 'TB0003', 'PNL QTR INNER LOWER RH', 'D46T', '1~3\n', 'TB', 10),
(43, 'TB0004', 'HOUSING RR COMBINATION LH', 'BOYUE', '\n', 'TB', 10),
(44, 'TB0005', 'HOUSING RR COMBINATION RH', 'BOYUE', '\n', 'TB', 10),
(45, 'TB0006', 'REINF TAIL GATE HINGE LH', 'BOYUE', '\n', 'TB', 10),
(46, 'None', 'None', 'None', 'None', 'None', 0);

-- --------------------------------------------------------

--
-- Table structure for table `type_pattern`
--

CREATE TABLE `type_pattern` (
  `id` int(11) NOT NULL,
  `pattern_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_pattern`
--

INSERT INTO `type_pattern` (`id`, `pattern_name`) VALUES
(1, 'a'),
(2, 'b'),
(3, 'c');

-- --------------------------------------------------------

--
-- Table structure for table `type_production_downtime`
--

CREATE TABLE `type_production_downtime` (
  `id` int(11) NOT NULL,
  `production_downtime` varchar(255) DEFAULT NULL,
  `department_id` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_production_downtime`
--

INSERT INTO `type_production_downtime` (`id`, `production_downtime`, `department_id`) VALUES
(1, 'Manpower shortage', 3),
(2, 'Transfer manpower', 3),
(3, 'Rearrange Manpower', 3),
(4, 'Briefing / 5s / On machine', 3),
(5, 'QC Check part', 3),
(6, 'Quality Check Online', 3),
(7, 'Trial', 3),
(8, 'Forklift', 3),
(9, 'Setting coil', 3),
(10, 'Cat line', 3),
(11, 'Material setting', 3),
(12, 'Mis loading', 3),
(13, 'Part urgent', 3),
(14, 'Maintenance facilities', 3),
(15, 'Run manual', 3),
(16, 'Blackout', 3),
(17, 'Part setting', 3),
(18, 'Die setting', 3),
(19, 'Destacker setting', 3),
(20, 'Pallet setting', 3),
(21, 'Vacuum error', 3),
(22, 'Centering part sensor problem', 3),
(23, 'Belting motor problem', 3),
(24, 'Robot setting', 3),
(25, 'Reteaching', 3),
(26, 'Die Change (Extra Downtime)', 3),
(27, 'Recycle process', 3),
(28, 'Double stamp', 3),
(29, 'Loading / Unloading - Big Parts', 3),
(30, 'Cushion pin issues', 3),
(31, 'Glove shortage', 3),
(32, 'Finger problem', 3);

-- --------------------------------------------------------

--
-- Table structure for table `type_qc_downtime`
--

CREATE TABLE `type_qc_downtime` (
  `id` int(11) NOT NULL,
  `qc_downtime` varchar(255) DEFAULT NULL,
  `department_id` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_qc_downtime`
--

INSERT INTO `type_qc_downtime` (`id`, `qc_downtime`, `department_id`) VALUES
(1, 'Buy-off part Inspection', 6),
(2, 'Quality issue inspection', 6);

-- --------------------------------------------------------

--
-- Table structure for table `type_role`
--

CREATE TABLE `type_role` (
  `id` int(11) NOT NULL,
  `role` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_role`
--

INSERT INTO `type_role` (`id`, `role`) VALUES
(1, 'Enginner'),
(2, 'Technician'),
(3, 'manager');

-- --------------------------------------------------------

--
-- Table structure for table `type_shift`
--

CREATE TABLE `type_shift` (
  `id` int(11) NOT NULL,
  `shift_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `type_shift`
--

INSERT INTO `type_shift` (`id`, `shift_name`) VALUES
(1, 'a'),
(2, 'b'),
(3, 'c');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `ic_number` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `status`, `role_id`, `ic_number`) VALUES
(1, 'Test', 'Test123', '7465737440313233', 'Unblock', 3, '123456789009');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_actual_seq`
-- (See below for the actual view)
--
CREATE TABLE `v_actual_seq` (
`date` date
,`time` time
,`part_no` varchar(255)
,`part_name` varchar(255)
,`qty_plan` varchar(20)
,`start_time` datetime
,`end_time` datetime
,`actual_time` varchar(20)
,`qty_total` varchar(20)
,`gsph` varchar(20)
,`total_dt` varchar(20)
,`dies_change` varchar(20)
,`dies_change_dt` varchar(20)
,`no_production_dt` varchar(20)
,`production_dt` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_current_part`
-- (See below for the actual view)
--
CREATE TABLE `v_current_part` (
`id` int(11)
,`part_no` varchar(255)
,`part_name` varchar(255)
,`line` varchar(255)
,`model` varchar(255)
,`process` varchar(255)
,`cycle_time_sec` int(11)
,`qty_rework` varchar(25)
,`qty_scrap` varchar(25)
,`qty_ok` varchar(20)
,`gsph` varchar(20)
,`qty_total` varchar(20)
,`shift_name` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_daily_derive`
-- (See below for the actual view)
--
CREATE TABLE `v_daily_derive` (
`id` int(11)
,`date` date
,`gsph` varchar(20)
,`qty_achievement` varchar(20)
,`oee` varchar(20)
,`availability` varchar(20)
,`utilization` varchar(20)
,`max_speed` varchar(20)
,`actual_speed` varchar(15)
,`speed_loss` varchar(15)
,`yield` varchar(20)
,`good_footage` varchar(15)
,`rejection_footage` varchar(15)
,`total_dt` varchar(10)
,`speed` varchar(5)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_downtime_report`
-- (See below for the actual view)
--
CREATE TABLE `v_downtime_report` (
`date` date
,`part_no` varchar(255)
,`shift_name` varchar(20)
,`time` time
,`downtime_period` varchar(20)
,`dies_change_downtime` varchar(20)
,`die_downtime` varchar(20)
,`machine_downtime_cause` varchar(255)
,`material_downtime` varchar(20)
,`production_downtime` varchar(255)
,`qc_downtime` varchar(255)
,`department_name` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_dt_daily`
-- (See below for the actual view)
--
CREATE TABLE `v_dt_daily` (
`id` int(11)
,`date` date
,`plan_production` varchar(20)
,`no_production` varchar(20)
,`dies_change` varchar(20)
,`production_dt` varchar(20)
,`dies_change_dt` varchar(20)
,`machine_dt` varchar(20)
,`dies_dt` varchar(20)
,`quality_dt` varchar(20)
,`material_dt` varchar(20)
,`qty_plan` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_front_panel`
-- (See below for the actual view)
--
CREATE TABLE `v_front_panel` (
`date` date
,`shift_id` int(11)
,`part_no_id` int(11)
,`plan_production` decimal(44,6)
,`no_production` decimal(44,6)
,`dies_change` decimal(44,6)
,`production_dt` decimal(44,6)
,`dies_change_dt` decimal(44,6)
,`machine_dt` decimal(44,6)
,`dies_dt` decimal(44,6)
,`quality_dt` decimal(44,6)
,`material_dt` decimal(44,6)
,`qty_plan` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_rework`
-- (See below for the actual view)
--
CREATE TABLE `v_rework` (
`id` int(11)
,`date` date
,`part_no` varchar(255)
,`shift_name` varchar(20)
,`crack` varchar(255)
,`necking` varchar(255)
,`dented` varchar(255)
,`wrinkle` varchar(255)
,`mekure` varchar(255)
,`burr` varchar(255)
,`dimension_ng` varchar(255)
,`other` varchar(255)
,`sum_rework` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_scrap`
-- (See below for the actual view)
--
CREATE TABLE `v_scrap` (
`id` int(11)
,`date` date
,`part_no` varchar(255)
,`shift_name` varchar(20)
,`crack` varchar(255)
,`necking` varchar(255)
,`dented` varchar(255)
,`wrinkle` varchar(255)
,`mekure` varchar(255)
,`burr` varchar(255)
,`dimension_ng` varchar(255)
,`other` varchar(255)
,`sum_scrap` double
);

-- --------------------------------------------------------

--
-- Structure for view `v_actual_seq`
--
DROP TABLE IF EXISTS `v_actual_seq`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_actual_seq`  AS  select `actual_sequence`.`date` AS `date`,`actual_sequence`.`time` AS `time`,`type_part_list`.`part_no` AS `part_no`,`type_part_list`.`part_name` AS `part_name`,`actual_sequence`.`qty_plan` AS `qty_plan`,`actual_sequence`.`start_time` AS `start_time`,`actual_sequence`.`end_time` AS `end_time`,`actual_sequence`.`actual_time` AS `actual_time`,`actual_sequence`.`qty_total` AS `qty_total`,`actual_sequence`.`gsph` AS `gsph`,`actual_sequence`.`total_dt` AS `total_dt`,`actual_sequence`.`dies_change` AS `dies_change`,`actual_sequence`.`dies_change_dt` AS `dies_change_dt`,`actual_sequence`.`no_production_dt` AS `no_production_dt`,`actual_sequence`.`production_dt` AS `production_dt` from (`actual_sequence` join `type_part_list` on(`actual_sequence`.`part_no_id` = `type_part_list`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_current_part`
--
DROP TABLE IF EXISTS `v_current_part`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_current_part`  AS  select `derive`.`id` AS `id`,`type_part_list`.`part_no` AS `part_no`,`type_part_list`.`part_name` AS `part_name`,`type_part_list`.`line` AS `line`,`type_part_list`.`model` AS `model`,`type_part_list`.`process` AS `process`,`type_part_list`.`cycle_time_sec` AS `cycle_time_sec`,`derive`.`qty_rework` AS `qty_rework`,`derive`.`qty_scrap` AS `qty_scrap`,`derive`.`qty_ok` AS `qty_ok`,`derive`.`gsph` AS `gsph`,`derive`.`qty_actual` AS `qty_total`,`type_shift`.`shift_name` AS `shift_name` from ((`derive` join `type_shift` on(`derive`.`shift_id` = `type_shift`.`id`)) join `type_part_list` on(`derive`.`part_no_id` = `type_part_list`.`id`)) order by `derive`.`id` desc limit 1 ;

-- --------------------------------------------------------

--
-- Structure for view `v_daily_derive`
--
DROP TABLE IF EXISTS `v_daily_derive`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_daily_derive`  AS  select `derive`.`id` AS `id`,`derive`.`date` AS `date`,`derive`.`gsph` AS `gsph`,ifnull(`derive`.`qty_achievement`,'0') AS `qty_achievement`,`derive`.`oee` AS `oee`,`derive`.`availability` AS `availability`,`derive`.`utilization` AS `utilization`,`derive`.`max_speed` AS `max_speed`,`derive`.`actual_speed` AS `actual_speed`,`derive`.`speed_loss` AS `speed_loss`,`derive`.`yield` AS `yield`,`derive`.`good_footage` AS `good_footage`,`derive`.`rejection_footage` AS `rejection_footage`,`derive`.`total_dt` AS `total_dt`,`derive`.`speed` AS `speed` from `derive` where `derive`.`date` = curdate() ;

-- --------------------------------------------------------

--
-- Structure for view `v_downtime_report`
--
DROP TABLE IF EXISTS `v_downtime_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_downtime_report`  AS  select `downtime_report`.`date` AS `date`,ifnull(`type_part_list`.`part_no`,NULL) AS `part_no`,ifnull(`type_shift`.`shift_name`,'-') AS `shift_name`,`downtime_report`.`time` AS `time`,`downtime_report`.`downtime_period` AS `downtime_period`,ifnull(`type_dies_change_downtime`.`dies_change_downtime`,'-') AS `dies_change_downtime`,ifnull(`type_die_downtime`.`die_downtime`,'-') AS `die_downtime`,ifnull(`type_machine_downtime`.`machine_downtime_cause`,'-') AS `machine_downtime_cause`,ifnull(`type_material_downtime`.`material_downtime`,'-') AS `material_downtime`,ifnull(`type_production_downtime`.`production_downtime`,'-') AS `production_downtime`,ifnull(`type_qc_downtime`.`qc_downtime`,'-') AS `qc_downtime`,ifnull(`type_department`.`department_name`,NULL) AS `department_name` from (((((((((`downtime_report` left join `type_part_list` on(`downtime_report`.`part_no_id` = `type_part_list`.`id`)) left join `type_shift` on(`downtime_report`.`shift_id` = `type_shift`.`id`)) left join `type_dies_change_downtime` on(`downtime_report`.`problem_dies_change` = `type_dies_change_downtime`.`id`)) left join `type_die_downtime` on(`downtime_report`.`problem_die` = `type_die_downtime`.`id`)) left join `type_machine_downtime` on(`downtime_report`.`problem_machine` = `type_machine_downtime`.`id`)) left join `type_material_downtime` on(`downtime_report`.`problem_material` = `type_material_downtime`.`id`)) left join `type_production_downtime` on(`downtime_report`.`problem_production` = `type_production_downtime`.`id`)) left join `type_qc_downtime` on(`downtime_report`.`problem_qc` = `type_qc_downtime`.`id`)) left join `type_department` on(`downtime_report`.`pic_id` = `type_department`.`id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_dt_daily`
--
DROP TABLE IF EXISTS `v_dt_daily`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_dt_daily`  AS  select `real_time`.`id` AS `id`,`real_time`.`date` AS `date`,ifnull(`real_time`.`plan_production`,'00:00:00') AS `plan_production`,ifnull(`real_time`.`no_production`,'00:00:00') AS `no_production`,ifnull(`real_time`.`dies_change`,'00:00:00') AS `dies_change`,ifnull(`real_time`.`production_dt`,'00:00:00') AS `production_dt`,ifnull(`real_time`.`dies_change_dt`,'00:00:00') AS `dies_change_dt`,ifnull(`real_time`.`machine_dt`,'00:00:00') AS `machine_dt`,ifnull(`real_time`.`dies_dt`,'00:00:00') AS `dies_dt`,ifnull(`real_time`.`quality_dt`,'00:00:00') AS `quality_dt`,ifnull(`real_time`.`material_dt`,'00:00:00') AS `material_dt`,ifnull(`real_time`.`qty_plan`,'00:00:00') AS `qty_plan` from `real_time` where `real_time`.`date` = curdate() ;

-- --------------------------------------------------------

--
-- Structure for view `v_front_panel`
--
DROP TABLE IF EXISTS `v_front_panel`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_front_panel`  AS  select `real_time`.`date` AS `date`,`real_time`.`shift_id` AS `shift_id`,`real_time`.`part_no_id` AS `part_no_id`,sum(time_to_sec(ifnull(`real_time`.`plan_production`,0))) AS `plan_production`,sum(time_to_sec(ifnull(`real_time`.`no_production`,0))) AS `no_production`,sum(time_to_sec(ifnull(`real_time`.`dies_change`,0))) AS `dies_change`,sum(time_to_sec(ifnull(`real_time`.`production_dt`,0))) AS `production_dt`,sum(time_to_sec(ifnull(`real_time`.`dies_change_dt`,0))) AS `dies_change_dt`,sum(time_to_sec(ifnull(`real_time`.`machine_dt`,0))) AS `machine_dt`,sum(time_to_sec(ifnull(`real_time`.`dies_dt`,0))) AS `dies_dt`,sum(time_to_sec(ifnull(`real_time`.`quality_dt`,0))) AS `quality_dt`,sum(time_to_sec(ifnull(`real_time`.`material_dt`,0))) AS `material_dt`,`real_time`.`qty_plan` AS `qty_plan` from (`real_time` join `fp_setting` on(`real_time`.`date` = `fp_setting`.`date` and `real_time`.`part_no_id` = `fp_setting`.`part_no_id` and `real_time`.`shift_id` = `fp_setting`.`shift_id`)) ;

-- --------------------------------------------------------

--
-- Structure for view `v_rework`
--
DROP TABLE IF EXISTS `v_rework`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_rework`  AS  select `rework_report`.`id` AS `id`,`rework_report`.`date` AS `date`,ifnull(`type_part_list`.`part_no`,NULL) AS `part_no`,ifnull(`type_shift`.`shift_name`,'-') AS `shift_name`,`rework_report`.`crack` AS `crack`,`rework_report`.`necking` AS `necking`,`rework_report`.`dented` AS `dented`,`rework_report`.`wrinkle` AS `wrinkle`,`rework_report`.`mekure` AS `mekure`,`rework_report`.`burr` AS `burr`,`rework_report`.`dimension_ng` AS `dimension_ng`,`rework_report`.`other` AS `other`,sum(`rework_report`.`crack`) + sum(`rework_report`.`necking`) + sum(`rework_report`.`dented`) + sum(`rework_report`.`wrinkle`) + sum(`rework_report`.`seizure`) + sum(`rework_report`.`mekure`) + sum(`rework_report`.`burr`) + sum(`rework_report`.`dimension_ng`) + sum(`rework_report`.`other`) AS `sum_rework` from ((`rework_report` join `type_part_list` on(`rework_report`.`part_no_id` = `type_part_list`.`id`)) join `type_shift` on(`rework_report`.`shift_id` = `type_shift`.`id`)) group by `rework_report`.`id` ;

-- --------------------------------------------------------

--
-- Structure for view `v_scrap`
--
DROP TABLE IF EXISTS `v_scrap`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_scrap`  AS  select `scrap_report`.`id` AS `id`,`scrap_report`.`date` AS `date`,ifnull(`type_part_list`.`part_no`,NULL) AS `part_no`,ifnull(`type_shift`.`shift_name`,'-') AS `shift_name`,`scrap_report`.`crack` AS `crack`,`scrap_report`.`necking` AS `necking`,`scrap_report`.`dented` AS `dented`,`scrap_report`.`wrinkle` AS `wrinkle`,`scrap_report`.`mekure` AS `mekure`,`scrap_report`.`burr` AS `burr`,`scrap_report`.`dimension_ng` AS `dimension_ng`,`scrap_report`.`other` AS `other`,sum(`scrap_report`.`crack`) + sum(`scrap_report`.`necking`) + sum(`scrap_report`.`dented`) + sum(`scrap_report`.`wrinkle`) + sum(`scrap_report`.`seizure`) + sum(`scrap_report`.`mekure`) + sum(`scrap_report`.`burr`) + sum(`scrap_report`.`dimension_ng`) + sum(`scrap_report`.`other`) AS `sum_scrap` from ((`scrap_report` join `type_part_list` on(`scrap_report`.`part_no_id` = `type_part_list`.`id`)) join `type_shift` on(`scrap_report`.`shift_id` = `type_shift`.`id`)) group by `scrap_report`.`id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actual_sequence`
--
ALTER TABLE `actual_sequence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `part_no_id` (`part_no_id`);

--
-- Indexes for table `daily_gsph`
--
ALTER TABLE `daily_gsph`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_id` (`shift_id`,`part_no_id`),
  ADD KEY `part_no_id` (`part_no_id`);

--
-- Indexes for table `derive`
--
ALTER TABLE `derive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_id` (`shift_id`,`part_no_id`),
  ADD KEY `part_no_id` (`part_no_id`);

--
-- Indexes for table `downtime_report`
--
ALTER TABLE `downtime_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `problem_dies_change` (`problem_dies_change`),
  ADD KEY `problem_machine` (`problem_machine`),
  ADD KEY `problem_material` (`problem_material`),
  ADD KEY `problem_production` (`problem_production`),
  ADD KEY `problem_qc` (`problem_qc`),
  ADD KEY `pic_id` (`pic_id`),
  ADD KEY `part_no_id` (`part_no_id`) USING BTREE,
  ADD KEY `problem_die` (`problem_die`) USING BTREE;

--
-- Indexes for table `fp_setting`
--
ALTER TABLE `fp_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `machine_info`
--
ALTER TABLE `machine_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `real_time`
--
ALTER TABLE `real_time`
  ADD PRIMARY KEY (`id`),
  ADD KEY `part_no_id` (`part_no_id`),
  ADD KEY `shift_id` (`shift_id`);

--
-- Indexes for table `rework_report`
--
ALTER TABLE `rework_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `part_no_id` (`part_no_id`);

--
-- Indexes for table `scrap_report`
--
ALTER TABLE `scrap_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `part_no_id` (`part_no_id`);

--
-- Indexes for table `type_defect`
--
ALTER TABLE `type_defect`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_department`
--
ALTER TABLE `type_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_dies_change_downtime`
--
ALTER TABLE `type_dies_change_downtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `type_die_downtime`
--
ALTER TABLE `type_die_downtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `type_machine_downtime`
--
ALTER TABLE `type_machine_downtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `type_material_downtime`
--
ALTER TABLE `type_material_downtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `type_part_list`
--
ALTER TABLE `type_part_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_pattern`
--
ALTER TABLE `type_pattern`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_production_downtime`
--
ALTER TABLE `type_production_downtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `type_qc_downtime`
--
ALTER TABLE `type_qc_downtime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`);

--
-- Indexes for table `type_role`
--
ALTER TABLE `type_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_shift`
--
ALTER TABLE `type_shift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actual_sequence`
--
ALTER TABLE `actual_sequence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `daily_gsph`
--
ALTER TABLE `daily_gsph`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `derive`
--
ALTER TABLE `derive`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `downtime_report`
--
ALTER TABLE `downtime_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `fp_setting`
--
ALTER TABLE `fp_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `machine_info`
--
ALTER TABLE `machine_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `real_time`
--
ALTER TABLE `real_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `rework_report`
--
ALTER TABLE `rework_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `scrap_report`
--
ALTER TABLE `scrap_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `type_defect`
--
ALTER TABLE `type_defect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `type_department`
--
ALTER TABLE `type_department`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `type_dies_change_downtime`
--
ALTER TABLE `type_dies_change_downtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `type_die_downtime`
--
ALTER TABLE `type_die_downtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `type_machine_downtime`
--
ALTER TABLE `type_machine_downtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `type_material_downtime`
--
ALTER TABLE `type_material_downtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `type_part_list`
--
ALTER TABLE `type_part_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `type_pattern`
--
ALTER TABLE `type_pattern`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `type_production_downtime`
--
ALTER TABLE `type_production_downtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `type_qc_downtime`
--
ALTER TABLE `type_qc_downtime`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `type_role`
--
ALTER TABLE `type_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `type_shift`
--
ALTER TABLE `type_shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `actual_sequence`
--
ALTER TABLE `actual_sequence`
  ADD CONSTRAINT `actual_sequence_ibfk_2` FOREIGN KEY (`part_no_id`) REFERENCES `type_part_list` (`id`);

--
-- Constraints for table `derive`
--
ALTER TABLE `derive`
  ADD CONSTRAINT `derive_ibfk_1` FOREIGN KEY (`part_no_id`) REFERENCES `type_part_list` (`id`),
  ADD CONSTRAINT `derive_ibfk_2` FOREIGN KEY (`shift_id`) REFERENCES `type_shift` (`id`);

--
-- Constraints for table `downtime_report`
--
ALTER TABLE `downtime_report`
  ADD CONSTRAINT `downtime_report_ibfk_1` FOREIGN KEY (`problem_die`) REFERENCES `type_die_downtime` (`id`),
  ADD CONSTRAINT `downtime_report_ibfk_2` FOREIGN KEY (`problem_dies_change`) REFERENCES `type_dies_change_downtime` (`id`),
  ADD CONSTRAINT `downtime_report_ibfk_3` FOREIGN KEY (`problem_machine`) REFERENCES `type_machine_downtime` (`id`),
  ADD CONSTRAINT `downtime_report_ibfk_4` FOREIGN KEY (`problem_material`) REFERENCES `type_material_downtime` (`id`),
  ADD CONSTRAINT `downtime_report_ibfk_5` FOREIGN KEY (`problem_production`) REFERENCES `type_production_downtime` (`id`),
  ADD CONSTRAINT `downtime_report_ibfk_6` FOREIGN KEY (`problem_qc`) REFERENCES `type_qc_downtime` (`id`),
  ADD CONSTRAINT `downtime_report_ibfk_7` FOREIGN KEY (`pic_id`) REFERENCES `type_department` (`id`),
  ADD CONSTRAINT `downtime_report_ibfk_8` FOREIGN KEY (`part_no_id`) REFERENCES `type_part_list` (`id`),
  ADD CONSTRAINT `downtime_report_ibfk_9` FOREIGN KEY (`shift_id`) REFERENCES `type_shift` (`id`);

--
-- Constraints for table `real_time`
--
ALTER TABLE `real_time`
  ADD CONSTRAINT `real_time_ibfk_1` FOREIGN KEY (`part_no_id`) REFERENCES `type_part_list` (`id`),
  ADD CONSTRAINT `real_time_ibfk_2` FOREIGN KEY (`shift_id`) REFERENCES `type_shift` (`id`);

--
-- Constraints for table `rework_report`
--
ALTER TABLE `rework_report`
  ADD CONSTRAINT `rework_report_ibfk_1` FOREIGN KEY (`part_no_id`) REFERENCES `type_part_list` (`id`),
  ADD CONSTRAINT `rework_report_ibfk_2` FOREIGN KEY (`shift_id`) REFERENCES `type_shift` (`id`);

--
-- Constraints for table `type_dies_change_downtime`
--
ALTER TABLE `type_dies_change_downtime`
  ADD CONSTRAINT `type_dies_change_downtime_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `type_department` (`id`);

--
-- Constraints for table `type_die_downtime`
--
ALTER TABLE `type_die_downtime`
  ADD CONSTRAINT `type_die_downtime_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `type_department` (`id`);

--
-- Constraints for table `type_machine_downtime`
--
ALTER TABLE `type_machine_downtime`
  ADD CONSTRAINT `type_machine_downtime_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `type_department` (`id`);

--
-- Constraints for table `type_material_downtime`
--
ALTER TABLE `type_material_downtime`
  ADD CONSTRAINT `type_material_downtime_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `type_department` (`id`);

--
-- Constraints for table `type_production_downtime`
--
ALTER TABLE `type_production_downtime`
  ADD CONSTRAINT `type_production_downtime_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `type_department` (`id`);

--
-- Constraints for table `type_qc_downtime`
--
ALTER TABLE `type_qc_downtime`
  ADD CONSTRAINT `type_qc_downtime_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `type_department` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `type_role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
